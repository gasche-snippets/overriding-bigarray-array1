(**
  In http://gallium.inria.fr/blog/overriding-submodules
  we show how to override submodules by using substitutive equalities
  in module signatures.

  Leonid Rozenberg asks how to do the same thing for the submodules of
  the Bigarray module. A naive attempt fails:

    include (Bigarray : module type of Bigarray
                        with module Array1 = Bigarray.Array1)
  > 
  > Error: In this `with' constraint, the new definition of Array1
  >      does not match its original definition in the constrained signature:
  >      ...
  >      Values do not match:
  >        val create :
  >          ('a, 'b) Bigarray.kind ->
  >          'c Bigarray.layout -> int -> ('a, 'b, 'c) t
  >      is not included in
  >        val create : ('a, 'b) kind -> 'c layout -> int -> ('a, 'b, 'c) t

  The problem seems to be that the type 'kind' in (module type of
  Bigarray) is not *equal* to the type Bigarray.kind. But adding one
  more equality does not work:

    include (Bigarray : module type of Bigarray
                        with type ('a, 'b) kind = ('a, 'b) Bigarray.kind
                        and module Array1 = Bigarray.Array1)
  > 
  > Error: This variant or record definition does not match that of type
  >        ('a, 'b) Bigarray.kind
  >      The types for field Float32 are not equal.

  The problem comes from the fact that kind is a GADT type using phantom types
  float32_elt, float64_elt, etc. declared in the following fashion:

    module Bigarray = struct
      type float32_elt = Float32_elt
      type float64_elt = Float64_elt
      type int8_signed = Int8_signed
    
      type ('a, 'b) kind =
        | Float32 : (float, float32_elt) kind
        | Float64 : (float, float64_elt) kind
        | Int8_signed : (int, int8_signed) kind
        | ...
    end

  as the phantom types foo_elt are defined *inside* the Bigarray
  module, the type float32_elt in (module type of Bigarray) is not
  equal to the type Bigarray.float32_elt ("module type" does not use
  the strengthened signature).

  To solve the issue, we manually strengthen the definition of all
  those phantom types:

    include (Bigarray : module type of Bigarray
                        with type float32_elt = Bigarray.float32_elt
                        and type float64_elt = Bigarray.float64_elt
                        and ...
                        and type ('a, 'b) kind = ('a, 'b) Bigarray.kind
                        and module Array1 = Bigarray.Array1)
 *)

module BigarrayExt = struct
  include
    (Bigarray : module type of Bigarray
    
    (* manual strengthening: preserve equalities of all phantom types *)
    with type float32_elt = Bigarray.float32_elt
    and type float64_elt = Bigarray.float64_elt
    and type int8_signed_elt = Bigarray.int8_signed_elt
    and type int8_unsigned_elt = Bigarray.int8_unsigned_elt
    and type int16_signed_elt = Bigarray.int16_signed_elt
    and type int16_unsigned_elt = Bigarray.int16_unsigned_elt
    and type int32_elt = Bigarray.int32_elt
    and type int64_elt = Bigarray.int64_elt
    and type int_elt = Bigarray.int_elt
    and type nativeint_elt = Bigarray.nativeint_elt
    and type complex32_elt = Bigarray.complex32_elt
    and type complex64_elt = Bigarray.complex64_elt
    and type c_layout = Bigarray.c_layout
    and type fortran_layout = Bigarray.fortran_layout

    (* manual strengthening: export equalities of GADTs *)
    and type 'c layout = 'c Bigarray.layout
    and type ('a, 'b) kind = ('a, 'b) Bigarray.kind

    (* the signature is now strengthened,
       and we can substitue modules with themselves *)
    and module Array1 := Bigarray.Array1
  )

  (* so now Array1 can be extended *)
  module Array1 = struct
      include Bigarray.Array1
      let foo = ()
  end
end
